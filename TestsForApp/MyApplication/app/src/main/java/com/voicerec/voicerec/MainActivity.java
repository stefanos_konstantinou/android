package com.voicerec.voicerec;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.service.voice.AlwaysOnHotwordDetector;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Policy;
import java.util.ArrayList;

//import java.io.OutputStreamWriter;
//import java.text.BreakIterator;
//import java.util.Locale;
//import android.hardware.Camera.Parameters;
//import android.hardware.Camera;
//import android.graphics.Camera;
//import android.location.Location;
//import android.telephony.SmsMessage;
//import android.util.Log;
//import android.content.BroadcastReceiver;
//import android.media.AudioManager;
//import android.text.InputFilter;
//import android.widget.TextView;
//import android.media.RingtoneManager;


public class MainActivity extends AppCompatActivity {

    private static final int VOICE_RECOGNITION_RESULT = 1000;
    public String MagicWord;
    //private Camera camera;
    private boolean hasFlash;
    private Policy.Parameters parameters;
    private AudioManager audioManager;
    private RingtoneManager ringtoneManager;
    private AudioManager audio;
    SharedPreferences storage;
    AlwaysOnHotwordDetector alwaysOnHotwordDetector;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VoiceRec();
        onSubmitMagicWord();
        Email();
        SendEmail();
        onReceivingSMS();
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);


    }
    @Override
    protected void onPause() {
        super.onPause();
        //onReceivingSMS();
    }

    String LocationLink;
    GPSTracker gps;
    SMSBroadcastReceiver SMS;
    String message;
    String email;
    String user = "droidapptest94@gmail.com";
    String pass = "androidtest"; //enter password
    String filename = "magicword.txt";
    private void onSubmitMagicWord() {
        Button Submit = (Button) findViewById(R.id.SubmitButton);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText magic = (EditText) findViewById(R.id.editSpecialWord);
                try {
                    if (magic.length() == 0) {
                        Toast.makeText(getApplicationContext(), "Please enter a word!", Toast.LENGTH_SHORT).show();
                    } else {
                        MagicWord = magic.getText().toString();
                        magic.setText("");

                        FileOutputStream OutputStream;
                        OutputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                        OutputStream.write(MagicWord.getBytes());
                        OutputStream.close();
                        Toast.makeText(getApplicationContext(), MagicWord + " is now your default word", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "An error occured.Please try again", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void VoiceRec() {
        Button voice = (Button) findViewById(R.id.voicebtn);
        voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                try {
                    startActivityForResult(intent, VOICE_RECOGNITION_RESULT);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error initializing speech engine", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case VOICE_RECOGNITION_RESULT:
                //do stuff with voice recognition
                if (resultCode == RESULT_OK && data != null) {

                    try {
                        ArrayList < String > result = data
                                .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        String mostLikelyThingHeard = result.get(0);
                        if (mostLikelyThingHeard.equalsIgnoreCase(MagicWord)) {
                            Toast.makeText(getApplicationContext(), "Word matches ", Toast.LENGTH_LONG).show();
                            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                            File k = new File("C:/Users/BlackDragon14/Desktop/BitBucketAndroid/TestsForApp/MyApplication/app/src/main/res/music", "A12Lumina.mp3"); // path is a file to /sdcard/media/ringtone
                            Uri ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                            Ringtone ringtone = RingtoneManager.getRingtone(getApplicationContext(), ringtoneUri);
                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
                            audioManager.setStreamVolume(AudioManager.STREAM_RING, 100, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND);
                            //ringtone.play();


                        } else if (MagicWord == null) {
                            Toast.makeText(getApplicationContext(), "No special word is set as default", Toast.LENGTH_SHORT).show();

                        } else if (!mostLikelyThingHeard.equals(MagicWord)) {
                            Toast.makeText(getApplicationContext(), "You said " + mostLikelyThingHeard + " instead of " + MagicWord, Toast.LENGTH_LONG).show();
                        }


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Error in recognizer", Toast.LENGTH_LONG).show();
                    }

                }
                break;
            default:
                break;
        }
    }

    private void Email() {
        Button EmailSub = (Button) findViewById(R.id.EmailBtn);
        EmailSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    EditText Uemail = (EditText) findViewById(R.id.UserEmail);
                    email = Uemail.getText().toString();
                    if (email.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Please enter an emergency E-mail!", Toast.LENGTH_SHORT).show();
                    } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) // Pattern that checks for possible mistakes
                    {
                        Toast.makeText(getApplicationContext(), "Wrong E-mail syntax given!", Toast.LENGTH_SHORT).show();
                    } else // If email is right is set as default emergency email
                    {
                        Toast.makeText(getApplicationContext(), "Emergency email is now set!", Toast.LENGTH_SHORT).show();
                        FileOutputStream OutputStream;
                        OutputStream = openFileOutput("emergemail.txt", Context.MODE_PRIVATE);
                        OutputStream.write(email.getBytes());
                        OutputStream.close();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void SendEmail() {
        final Button sendMail = (Button) this.findViewById(R.id.sendEmailbtn);
        sendMail.setOnClickListener(
                new Button.OnClickListener() {

                    public void onClick(View v) {
                        String TAG = "";
                         hasFlash=getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
                        if (hasFlash) {





                        }


                        //// TURNING ON MOBILE DATA //////
                        ConnectivityManager datamanager;
                        datamanager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                        try {
                            Method dataMtd=ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled",boolean.class);
                            dataMtd.setAccessible(true);
                            try {
                                dataMtd.invoke(datamanager,true);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }

                        ///////// TURNING ON MOBILE DATA //////


                        // Saving data in xml in the background
                        storage = getSharedPreferences(email, 0);
                        SharedPreferences.Editor editor = storage.edit();
                        editor.putString("email", email);
                        editor.commit();

                        FileInputStream fis = null;
                        try {
                            fis = getApplicationContext().openFileInput("emergemail.txt");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        InputStreamReader isr = new InputStreamReader(fis);
                        BufferedReader bufferedReader = new BufferedReader(isr);
                        StringBuilder sb = new StringBuilder();
                        String line = null;
                        try {
                            while ((line = bufferedReader.readLine()) != null) {
                                sb.append(line);
                            }
                            line = sb.toString();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (line != null) {
                            // Enter the sender email user name and password in gmail acc


                            gps = new GPSTracker(MainActivity.this);
                            if (gps.canGetLocation()) {

                                double latitude = gps.getLatitude();
                                double longitude = gps.getLongitude();
                                email = line;
                                // \n is for new line
                                LocationLink = "http://www.google.com/maps/place/" + latitude + "," + longitude;
                                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                                final MailSender la = new MailSender(user, pass, email, LocationLink);
                                try {
                                    la.send();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(getApplicationContext(), "Email Sent!", Toast.LENGTH_LONG).show();
                            } else {
                                // can't get location
                                // GPS or Network is not enabled
                                // Ask user to enable GPS/network in settings
                                gps.showSettingsAlert();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
        );
    }




    public void onReceivingSMS() {
        Intent intent = getIntent();
        message = intent.getStringExtra("message");
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        if (message != null) {

            FileInputStream fis = null;
            try {
                fis = getApplicationContext().openFileInput("magicword.txt");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                    line = sb.toString();
                    if (message.equalsIgnoreCase(line)) {
                        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        wifiManager.setWifiEnabled(true);
                        Toast.makeText(getApplicationContext(), " Magic Word matches message received  ", Toast.LENGTH_LONG).show();
                        FileInputStream fisi = null;
                        try {
                            fisi = getApplicationContext().openFileInput("emergemail.txt");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        InputStreamReader isra = new InputStreamReader(fisi);
                        BufferedReader bufferedReadera = new BufferedReader(isra);
                        StringBuilder sba = new StringBuilder();
                        String linea = null;
                        try {
                            while ((linea = bufferedReadera.readLine()) != null) {
                                sba.append(linea);
                            }
                            linea = sba.toString();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (linea != null) {
                            // Enter the sender email user name and password in gmail acc


                            gps = new GPSTracker(MainActivity.this);
                            if (gps.canGetLocation()) {

                                double latitude = gps.getLatitude();
                                double longitude = gps.getLongitude();
                                email = linea;
                                // \n is for new line
                                LocationLink = "http://www.google.com/maps/place/" + latitude + "," + longitude;
                                final MailSender la = new MailSender(user, pass, email, LocationLink);
                                Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                                new java.util.Timer().schedule(new java.util.TimerTask() {
                                                                   @Override public void run() {

                                                                       try {
                                                                           la.send();
                                                                       } catch (Exception e) {
                                                                           e.printStackTrace();
                                                                       }
                                                                   }
                                                               }, 10000

                                );

                                Toast.makeText(getApplicationContext(), "Email Sent!", Toast.LENGTH_LONG).show();
                            } else {
                                // can't get location
                                // GPS or Network is not enabled
                                // Ask user to enable GPS/network in settings
                                gps.showSettingsAlert();
                            }

                        } else {
                            Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                        }
                    }
                    //Toast.makeText(getApplicationContext(),line,Toast.LENGTH_LONG).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }



    }




    


}